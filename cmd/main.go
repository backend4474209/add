package main

import (
	pb "client/genproto"
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

const (
	x = 10
	y = 12
)

func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalln("error is while dial", err.Error())
	}

	defer conn.Close()

	c := pb.NewAddServiceClient(conn)

	resp, err := c.Add(context.Background(), &pb.AddRequest{
		X: int32(x),
		Y: int32(y),
	})
	if err != nil {
		log.Println("error is while add ######", err.Error())
	}

	log.Println("response -> sum = ", resp.GetSum())
}
